package lib1.lib2.lib3;

public interface _Positionable
{
	default  void showPosition()
	{
		System.out.println("ShowPosition() is calling!");
	}
}