/**
 * @author 黎夜之梦
 *
 */
package $01_mes;
/*

常用的RDBMS的JDBC驱动程序类名
Oracle:		oracle.jdbc.driver.OracleDriver
MySQL:		com.mysql.jdbc.Driver
SQL Server:	com.microsoft.sqlserver.jdbc.SQLServerDriver
Access:		sun.jdbc.odbc.JdbcDriver
Java DB:	org.apache.derby.jdbc.EmbeddedDriver

常用的RDBMS的JDBC URL
Oracle:		jdbc:oracle:thin:@host:port:db_name
MySQL:		jdbc:mysql://host:port/db_name
SQL Server:	jdbc:microsoft:sqlserver://host:port;DatabaseName=db_name
Access:		jdbc:odbc:driver={Microsoft Access Driver(*.mdb,*.accdb)};DBQ="path/db_filename"
Java DB:	jdbc:derby:path/db_foldername;create=true

*/