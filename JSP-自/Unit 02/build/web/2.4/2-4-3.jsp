<%-- 
    Document   : 2-4-3
    Created on : 2019-7-30, 10:41:53
    Author     : 黎夜之梦
--%>

<%@page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%--<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>--%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP taglib 指令演示</title>
    </head>
    <body>
        <table>
            <tr>
                <td>输出值</td>
            </tr>
           <%-- <c:forEach begin="1" end="10" var="num">
                <tr>
                    <td><c:out value="${num}"></c:out></td>
                </tr>
            </c:forEach>--%>
        </table>
    </body>
</html>
