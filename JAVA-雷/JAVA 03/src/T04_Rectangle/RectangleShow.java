package T04_Rectangle;

import function.Sharp;
import sharp.Rectangle;

/**
 * 设计Java类表示矩形，要求提由四个顶点坐标实例化，
 * 或由边长度和高度实例化，实现矩形周长和面积计算，
 * 在屏幕绘制绘制矩形？提示：要考虑该类可能会被用做父类。
 * @author 黎夜之梦
 *
 */

public class RectangleShow {

	public static void main(String[] args) {
		Sharp.nCubeJFrame(new Rectangle(), "矩形");
	}

}
