package T05_Round;

import function.Sharp;
import sharp.Round;

/**
 * 设计Java类表示圆形，
 * 要求提由圆心四个顶点坐标和半径长度实例化，
 * 实现圆形周长和面积计算，在屏幕绘制绘制圆形？
 * 提示：要考虑该类可能会被用做父类。
 * @author 黎夜之梦
 *
 */

public class RoundShow {

	public static void main(String[] args) {
		Sharp.nCubeJFrame(new Round(), "圆形");
	}

}
