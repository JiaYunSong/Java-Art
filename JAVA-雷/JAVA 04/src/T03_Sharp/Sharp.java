package T03_Sharp;

public abstract class Sharp {
	Point a[];
	Line l[];
	abstract public double Length();
	abstract public double Area();
}
