/**
 * 编写一个计算半径419米的圆的面积和周长？
 * @author 黎夜之梦
 *
 */
public class T04_RoundArea {
	public static void main(String[] args) {
		double r=419;
		System.out.println("圆的周长："+(2*Math.PI*r));
		System.out.println("圆的面积；"+(Math.PI*r*r));
	}
}
