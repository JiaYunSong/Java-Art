package topic;

//import function.Scan;

/**
 * 在实际的开发工作中，对字符串的处理是最常见的编程任务。本题目即是要求程序对用户输入的串进行处理。具体规则如下：
(1).把每个单词的首字母变为大写。
(2).把数字与字母之间用下划线字符（_）分开，使得更清晰。
(3).把单词中间有多个空格的调整为1个空格。
我们假设：用户输入的串中只有小写字母，空格和数字，不含其它的字母或符号。 
 每个单词间由1个或多个空格分隔。 
 假设用户输入的串长度不超过200个字符。 
 * @author 黎夜之梦
 *
 */

public class T38_StringProcess {

	public static void main(String[] args) {
//		String str=Scan.nextLine("String=");
//		String res="";
//		int n,k,ns,ks;
//		if(str.charAt(0)==' ') k=0;
//		else k=1;     //0为空格
//		if(str.charAt(0)>=47 && str.charAt(0)<=58) ks=1;
//		else ks=0;    //1为数字
//		n=k;
//		ns=ks;
//		for(char i:str.toCharArray()){
//			if(i==' ') k=0;
//			else k=1;
//			if(str.charAt(0)>=47 && str.charAt(0)<=58) ks=1;
//			else ks=0;
//			if(n==0 && k==0) continue;
//			else if(n==k)
//				if(ns==ks)
//					res=res+Character.toString(i);
//				else
//					res=res+"_"+Character.toString(i);
//			else if(n==0 && k==1 && ks==0)
//				res=res+Character.toString((char)(i-32));
//			else
//				res=res+Character.toString(i);
//			n=k;ns=ks;
//		}
//		System.out.println(res);
	}

}
