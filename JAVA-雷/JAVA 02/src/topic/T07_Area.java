package topic;
/**
 * 已知三角形的底边和高尺寸分别
 * 是56厘米、120厘米，计算该三角形的面积？
 * @author 黎夜之梦
 *
 */

public class T07_Area {
	public static void main(String[] args) {
		double a=56,h=120;
		System.out.println("三角形面积为:"+(a*h/2)+"cm");
	}
}
